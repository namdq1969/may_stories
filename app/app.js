import React, {Component} from 'react';
import {View, AsyncStorage} from 'react-native';
import {Router, Scene, Actions} from 'react-native-router-flux';
import {Provider, connect} from 'react-redux';
import configureStore from './store/configureStore';
import {persistStore} from 'redux-persist';
import {setCustomTextInput, setCustomText} from 'react-native-global-props';

import Home from './components/Home';
import RecordDetail from './components/RecordDetail';

const RouterWithRedux = connect()(Router);
const store = configureStore();
setCustomTextInput({
  style: {
    fontFamily: 'Nunito-Regular'
  }
});
setCustomText({
  style: {
    fontFamily: 'Nunito-Regular'
  }
});

export default class MayStories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rehydrated: false
    }
  }

  componentWillMount() {
    persistStore(store, {
      storage: AsyncStorage,
      blacklist: ['nav', 'request', 'component']
    }, () => {
      this.setState({rehydrated: true});
      console.log('finished restoration!!!');
    })
  }

  render() {
    if (!this.state.rehydrated) {
      return <View/>
    }
    return (
      <Provider store={store}>
        <RouterWithRedux>
          <Scene key={'root'} initial={true} tabs={false}>
            <Scene key={'home'} component={Home} hideTabBar={true} hideNavBar={true}/>
            <Scene key={'recordDetail'} component={RecordDetail} hideTabBar={true} hideNavBar={true}/>
          </Scene>
        </RouterWithRedux>
      </Provider>
    )
  }
}