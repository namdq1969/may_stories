import {combineReducers} from 'redux';
import nav, * as fromNav from './navReducer';

export default combineReducers({nav})
export const getNav = (state) => fromNav.getNav(state.nav);
