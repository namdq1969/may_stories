import React, {Component} from 'react'
import {
  View,
  ListView,
  Text,
  TextInput,
  Image,
  TouchableHighlight,
  StatusBar
} from 'react-native'
import moment from 'moment'
import {Actions} from 'react-native-router-flux'
import {sHeight, sWidth, sImages, sStyleSheets} from '../../global/constants'
import Ionicon from 'react-native-vector-icons/Ionicons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

import HomeTreding from '../SubComponents/HomeTrending'

export default class RecordDetail extends Component {
  constructor(props) {
    super(props);

    let ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1.id !== r2.id
    });
    
    let list = [{
      id: 1,
      title: 'Bạch tuyết và bảy chú lùn',
      duration: '1h 30m',
      author: 'May',
      createdAt: moment().format('DD / MM / YYYY'),
      thumbnail: 'https://s-media-cache-ak0.pinimg.com/originals/bb/49/19/bb4919e2383f18c565931a7f0490e89d.jpg',
      brief: 'Ngày xửa, ngày xưa, giữa mùa đông lạnh giá, bầu trời tuyết rơi nhiều và trắng như bông. Có 1 bà hoàng hậu ngồi bên khung cửa sổ bằng gỗ mụn khâu vá, bà mải ngắm nhìn tuyết rơi cho nên kim đâm phải tay bà, 3 giọt máu rơi xuống làn tuyết trắng xóa.'
    }, {
      id: 2,
      title: 'Bạch tuyết và bảy chú lùn',
      duration: '1h 30m',
      author: 'May',
      createdAt: moment().format('DD / MM / YYYY'),
      thumbnail: 'https://s-media-cache-ak0.pinimg.com/originals/bb/49/19/bb4919e2383f18c565931a7f0490e89d.jpg',
      brief: 'Ngày xửa, ngày xưa, giữa mùa đông lạnh giá, bầu trời tuyết rơi nhiều và trắng như bông. Có 1 bà hoàng hậu ngồi bên khung cửa sổ bằng gỗ mụn khâu vá, bà mải ngắm nhìn tuyết rơi cho nên kim đâm phải tay bà, 3 giọt máu rơi xuống làn tuyết trắng xóa.'
    }, {
      id: 3,
      title: 'Bạch tuyết và bảy chú lùn',
      duration: '1h 30m',
      author: 'May',
      createdAt: moment().format('DD / MM / YYYY'),
      thumbnail: 'https://s-media-cache-ak0.pinimg.com/originals/bb/49/19/bb4919e2383f18c565931a7f0490e89d.jpg',
      brief: 'Ngày xửa, ngày xưa, giữa mùa đông lạnh giá, bầu trời tuyết rơi nhiều và trắng như bông. Có 1 bà hoàng hậu ngồi bên khung cửa sổ bằng gỗ mụn khâu vá, bà mải ngắm nhìn tuyết rơi cho nên kim đâm phải tay bà, 3 giọt máu rơi xuống làn tuyết trắng xóa.'
    }, {
      id: 4,
      title: 'Bạch tuyết và bảy chú lùn',
      duration: '1h 30m',
      author: 'May',
      createdAt: moment().format('DD / MM / YYYY'),
      thumbnail: 'https://s-media-cache-ak0.pinimg.com/originals/bb/49/19/bb4919e2383f18c565931a7f0490e89d.jpg',
      brief: 'Ngày xửa, ngày xưa, giữa mùa đông lạnh giá, bầu trời tuyết rơi nhiều và trắng như bông. Có 1 bà hoàng hậu ngồi bên khung cửa sổ bằng gỗ mụn khâu vá, bà mải ngắm nhìn tuyết rơi cho nên kim đâm phải tay bà, 3 giọt máu rơi xuống làn tuyết trắng xóa.'
    }, {
      id: 5,
      title: 'Bạch tuyết và bảy chú lùn',
      duration: '1h 30m',
      author: 'May',
      createdAt: moment().format('DD / MM / YYYY'),
      thumbnail: 'https://s-media-cache-ak0.pinimg.com/originals/bb/49/19/bb4919e2383f18c565931a7f0490e89d.jpg',
      brief: 'Ngày xửa, ngày xưa, giữa mùa đông lạnh giá, bầu trời tuyết rơi nhiều và trắng như bông. Có 1 bà hoàng hậu ngồi bên khung cửa sổ bằng gỗ mụn khâu vá, bà mải ngắm nhìn tuyết rơi cho nên kim đâm phải tay bà, 3 giọt máu rơi xuống làn tuyết trắng xóa.'
    }]
    
    this.state = {
      dataSource: ds.cloneWithRows(list)
    }
  }
  
  _renderRow(rowData, sectionID, rowID) {
    return (
      <View style={{marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, borderColor: '#EEEEEE', borderWidth: 1}}>
        <View style={{height: 190, backgroundColor: 'white'}}>
          <View style={{margin: 10, height: 70, flexDirection: 'row'}}>
            <Image source={{uri: rowData.thumbnail}} style={{height: 70, width: 94}} resizeMode={Image.resizeMode.cover}/>
            <View style={{marginLeft: 10, justifyContent: 'center'}}>
              <Text style={{fontSize: 14, fontWeight: '700', color: '#E64A19'}}>{rowData.title}</Text>
              <Text style={{fontSize: 12, fontWeight: '300', color: '#FFAB91'}}>{`Thời lượng ${rowData.duration}`}</Text>
            </View>
          </View>
          <View style={{height: 60, marginLeft: 10, marginRight: 10}}>
            <Text style={{fontSize: 11, fontWeight: '300', color: '#757575'}}>{rowData.brief}</Text>
          </View>
          <View style={{marginTop: 10, marginLeft: 10, marginRight: 10, height: 1, backgroundColor: '#E0E0E0'}}/>
          <View style={{flex: 1, marginLeft: 15, marginRight: 15, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <FontAwesome name={'thumbs-o-up'} size={20} color={'#BDBDBD'}/>
              <Text style={{marginLeft: 5, color: '#BDBDBD', fontSize: 13}}>Like</Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <FontAwesome name={'comment-o'} size={20} color={'#BDBDBD'}/>
              <Text style={{marginLeft: 5, color: '#BDBDBD', fontSize: 13}}>Comment</Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <FontAwesome name={'share-square-o'} size={20} color={'#BDBDBD'}/>
              <Text style={{marginLeft: 5, color: '#BDBDBD', fontSize: 13}}>Share</Text>
            </View>
          </View>
        </View>
      </View>
    )
  }

  render() {
    return (
      <View style={{
        flex: 1
      }}>
        <StatusBar barStyle={'light-content'}/>
        <View style={{
          paddingTop: 20,
          height: 60,
          backgroundColor: '#EC407A',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between'
        }}>
          <TouchableHighlight style={{
            marginLeft: 10,
            height: 35,
            width: 35,
            alignItems: 'center',
            justifyContent: 'center'
          }} underlayColor={'transparent'} onPress={() => Actions.pop()}>
            <Ionicon name={'md-arrow-back'} size={24} color={'white'}/>
          </TouchableHighlight>
          <Image source={sImages.ms_logo}/>
          <TouchableHighlight style={{
            marginRight: 10,
            height: 34,
            width: 34,
            alignItems: 'center',
            justifyContent: 'center'
          }} underlayColor={'transparent'} onPress={() => {}}>
            <View style={{
              height: 28,
              width: 28,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 14,
              borderColor: 'white',
              borderWidth: 1
            }}>
              <Text style={{fontSize: 12, color: 'white', fontWeight: '600'}}>3</Text>
            </View>
          </TouchableHighlight>
        </View>
        <View style={{backgroundColor: '#F5F5F5', flex: 1}}>
          <ListView enableEmptySections={true} dataSource={this.state.dataSource} renderRow={this._renderRow.bind(this)} showsVerticalScrollIndicator={false}/>
        </View>

      </View>
    )
  }
}
