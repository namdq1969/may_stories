import React, {Component} from 'react'
import {
  View,
  ScrollView,
  Text,
  TextInput,
  Image,
  TouchableHighlight,
  StatusBar
} from 'react-native'
import moment from 'moment'
import {Actions} from 'react-native-router-flux'
import {sHeight, sWidth, sImages, sStyleSheets} from '../../global/constants'
import Ionicon from 'react-native-vector-icons/Ionicons'

import HomeTreding from '../SubComponents/HomeTrending'

export default class Home extends Component {
  constructor(props) {
    super(props);

  }

  render() {
    return (
      <View style={{
        flex: 1
      }}>
        <StatusBar barStyle={'light-content'}/>
        <View style={{
          paddingTop: 20,
          height: 60,
          backgroundColor: '#EC407A',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between'
        }}>
          <TouchableHighlight style={{
            marginLeft: 10,
            height: 35,
            width: 35,
            alignItems: 'center',
            justifyContent: 'center'
          }} underlayColor={'transparent'} onPress={() => {}}>
            <Ionicon name={'md-menu'} size={24} color={'white'}/>
          </TouchableHighlight>
          <Image source={sImages.ms_logo}/>
          <TouchableHighlight style={{
            marginRight: 10,
            height: 34,
            width: 34,
            alignItems: 'center',
            justifyContent: 'center'
          }} underlayColor={'transparent'} onPress={() => {}}>
            <View style={{
              height: 28,
              width: 28,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 14,
              borderColor: 'white',
              borderWidth: 1
            }}>
              <Text style={{fontSize: 12, color: 'white', fontWeight: '600'}}>3</Text>
            </View>
          </TouchableHighlight>
        </View>
        <ScrollView style={{
          backgroundColor: '#F5F5F5'
        }} showsVerticalScrollIndicator={false}>
          <HomeTreding/>

          <View style={{
            height: 35,
            marginLeft: 25,
            marginRight: 25,
            marginTop: 20,
            marginBottom: 30,
            backgroundColor: 'white',
            borderRadius: 3,
            borderColor: '#EEEEEE',
            borderWidth: 1
          }}>
            <TextInput style={{
              flex: 1,
              textAlign: 'center'
            }} autoCorrect={false} placeholder={'Search'} placeholderTextColor={'rgba(236, 64, 122, 0.8)'} onChangeText={(text) => {}}/>
          </View>
          <HomeTreding/>
          <HomeTreding/>
        </ScrollView>

      </View>
    )
  }
}
