import React, {Component} from 'react';
import {View, Text, ListView, TouchableHighlight, Image} from 'react-native'
import {sHeight, sWidth, sImages, sStyleSheets} from '../../../global/constants'
import Ionicon from 'react-native-vector-icons/Ionicons'
import moment from 'moment'
import {Actions} from 'react-native-router-flux';

export default class HomeTrending extends Component {
  constructor(props) {
    super(props);

    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1.id !== r2.id
    });
  }

  _renderRow(rowData, sectionID, rowID) {
    return (
      <TouchableHighlight underlayColor={'transparent'} onPress={() => Actions.recordDetail({data: {}})}>
        <View style={{
          margin: 10,
          height: sWidth * 0.6,
          width: sWidth * 0.6,
          borderColor: '#F50057',
          borderWidth: 1
        }}>
          <Image source={{
            uri: rowData.thumbnail
          }} style={{
            height: sWidth * 0.6 * 0.75,
            width: sWidth * 0.6 - 2
          }} resizeMode={Image.resizeMode.cover}>
            <View style={{
              backgroundColor: '#F50057',
              alignItems: 'center',
              alignSelf: 'flex-end',
              paddingLeft: 5,
              paddingRight: 5,
              paddingTop: 2,
              paddingBottom: 2,
              marginTop: 5,
              marginRight: 5
            }}>
              <Text style={{
                fontSize: 12,
                fontWeight: '400',
                color: 'white'
              }}>{rowData.duration}</Text>
            </View>
          </Image>
          <View style={{
            flex: 1,
            justifyContent: 'center'
          }}>
            <Text style={{
              marginLeft: 10,
              marginRight: 10,
              fontSize: 15,
              color: '#F06292',
              fontWeight: '600'
            }} numberOfLines={1} ellipsizeMode={'tail'}>{rowData.title}</Text>
            <Text style={{
              marginLeft: 10,
              marginRight: 10,
              fontSize: 13,
              color: '#F06292',
              fontWeight: '300'
            }} numberOfLines={1} ellipsizeMode={'tail'}>{`${rowData.author} - ${rowData.createdAt}`}</Text>
          </View>
        </View>
      </TouchableHighlight>
    )
  }

  render() {
    let tempSource = [];
    for (var i = 0; i < 5; i++) {
      let dataRow = {
        id: i,
        title: 'Bạch tuyết và bảy chú lùn',
        duration: '1h 30m',
        author: 'May',
        createdAt: moment().format('DD / MM / YYYY'),
        thumbnail: 'https://s-media-cache-ak0.pinimg.com/originals/bb/49/19/bb4919e2383f18c565931a7f0490e89d.jpg',
        brief: 'Ngày xửa, ngày xưa, giữa mùa đông lạnh giá, bầu trời tuyết rơi nhiều và trắng như bông. Có 1 bà hoàng hậu ngồi bên khung cửa sổ bằng gỗ mụn khâu vá, bà mải ngắm nhìn tuyết rơi cho nên kim đâm phải tay bà, 3 giọt máu rơi xuống làn tuyết trắng xóa.'
      }      
      tempSource.push(dataRow);
    }

    let dataSource = this.ds.cloneWithRows(tempSource);

    return (
      <View>
        <View style={{
          height: 40,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center'
        }}>
          <Text style={{
            marginLeft: 30,
            fontSize: 15,
            color: '#F50057'
          }}>Trending</Text>
          <TouchableHighlight style={{
            marginRight: 10,
            height: 35,
            width: 35,
            alignItems: 'center',
            justifyContent: 'center'
          }} underlayColor={'transparent'} onPress={() => {}}>
            <Ionicon name={'md-more'} color={'#F50057'} size={24}/>
          </TouchableHighlight>
        </View>
        <View style={{
          borderTopWidth: 1,
          borderBottomWidth: 1,
          borderColor: '#EEEEEE',
          backgroundColor: 'white'
        }}>
          <ListView horizontal={true} enableEmptySections={true} dataSource={dataSource} renderRow={this._renderRow.bind(this)} showsHorizontalScrollIndicator={false}/>
        </View>
      </View>
    )
  }
}