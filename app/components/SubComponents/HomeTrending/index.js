import HomeTrending from './HomeTrending';
import {connect} from 'react-redux';
import * as actions from './actions';
import {getNav} from '../../../reducers/rootReducer';

const mapStateToProps = (state) => ({
  ...getNav(state),
})

export default connect(mapStateToProps, actions)(HomeTrending);
