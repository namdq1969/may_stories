import {Dimensions} from 'react-native';

export const sHeight = Dimensions.get('window').height;
export const sWidth = Dimensions.get('window').width;

export const sImages = {
  ms_logo: require('../../Resources/ms_logo.png')
}

export const sStyleSheets = {
  shadow: {
    elevation: 2,
    backgroundColor: 'white',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 2,
    shadowOpacity: 0.3
  }
};